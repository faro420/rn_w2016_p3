#include "ucp.h" 
#include<arpa/inet.h>


void sys_call_error(const char *msg){
    perror(msg);
    fflush(stdout);
    exit(0);
}

void  init(config* setup){
        put=take=fullEntries=0;
        err=pthread_mutex_init(&mutex, NULL);
        assert(!err);
        err=pthread_cond_init(&empty, NULL);
        assert(!err);
        err=pthread_cond_init(&full, NULL);
        assert(!err);
        setup->cnt = 0;
        active = 1;
}

void cleanup(void){
        err=pthread_mutex_destroy(&mutex);
        assert(!err);
        err=pthread_cond_destroy(&empty);
        assert(!err);
        err=pthread_cond_destroy(&full);
        assert(!err);   
        printf("\nALL THREADS JOINED SUCCESSFULY\n");
        fflush(stdout);
}

 
 
uint32_t read_int(config * setup,int currentBuffer){
    uint32_t integer = 0;
    integer = integer | (setup->buffer[currentBuffer].element[3]<<24);
    integer = integer | (setup->buffer[currentBuffer].element[2]<<16);
    integer = integer | (setup->buffer[currentBuffer].element[1]<<8);
    integer = integer |  setup->buffer[currentBuffer].element[0];
    return integer;
}

void read_bytes(config* setup,int n, int currentBuffer,int* socket){
        bzero(setup->buffer[currentBuffer].element,400);
        n = read(*(socket),setup->buffer[currentBuffer].element,n);
        if (n < 0)sys_call_error("failed reading from the socket\n");
}

int getSocket(config* setup, int* currentBuffer){
    int socket;
    pthread_mutex_lock(&mutex);
    while (fullEntries == 0)   
        pthread_cond_wait(&full, &mutex);
    socket = setup->buffer[take].socket;
    *(currentBuffer)=take;
    take = (take + 1) % BUFFLEN;
    fullEntries--; 
    pthread_cond_broadcast(&empty);
    pthread_mutex_unlock( &mutex );
    return socket;
}
