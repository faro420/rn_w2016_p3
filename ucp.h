 /*
    @author: Mir Farshid Baha
    purpose: RNP-WS2016
    version: v0.9
*/
#ifndef UCP_H
#define UCP_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <pthread.h>
#include <assert.h>

#define BUFFLEN 4
#define _4_BYTES 4 

#define VERSION 0
#define CHECKSUM 1
#define TYPE 2
#define LENGTH 3
#define LOGIN 100
#define MESSAGE 101
#define UPDATE 102
#define LOGOUT 103
#define VERSION_NR 42
#define USER_FOUND 1

#define DEBUG

//structs
typedef struct {
    char element[400];
    int socket;
    uint32_t client_ip;
    uint16_t client_port;
}elements;

typedef struct{
    int sockfd, portno, error;
    struct sockaddr_in serv_addr;
    struct hostent *server;    
}connection;

typedef struct {
    char host_ip[20];
    uint16_t in_port;
    uint32_t in_ip_addr;
    uint16_t host_port;
    connection conn;
    uint32_t cnt;
    elements buffer[BUFFLEN];
}config;


//function prototypes
void* server(void*args);
void* client(void*args);
void* controller(void*args);
void login(config* setup, char* buffer);
void init(config* setup);
void cleanup(void);
void sys_call_error(const char *msg);
uint32_t read_int(config * setup, int currentBuffer);
void read_bytes(config* setup, int n, int currentBuffer,int* socket);
int getSocket(config* setup, int* currentBuffer);
void send_message(config* setup, char* buffer);

//declaration of synchronization elements
extern pthread_mutex_t mutex;
extern pthread_cond_t empty, full;
extern int active, err;
//buffer related structs and variables
extern int put, take, fullEntries;

#endif
