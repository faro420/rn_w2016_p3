 /*
    @author: Mir Farshid Baha
    purpose: RNP-WS2016
    integer: v0.9
*/
#include "ucp.h"

typedef struct{
    char username[20];
}users;

typedef struct{
    uint32_t ip;
    uint32_t port;
    uint32_t cnt;
    uint32_t length;
    char content[400];
}message;

//function prototypes
void broadcast_update(config*, uint32_t*, users*, uint8_t*);

void* controller(void*args){
    config * setup = ((config*)args);
    message msg;
    users list[BUFFLEN];
    int socket, currentBuffer;
    uint8_t i, next_name;
    uint32_t integer, length, type;
    uint32_t state =VERSION;
    integer =length= next_name= 0;
    msg.cnt=0;
    socket= getSocket(setup,&currentBuffer);
    while(active==1){
        switch(state){
            case VERSION: 
                        read_bytes(setup,_4_BYTES,currentBuffer,&socket);
                        integer = read_int(setup, currentBuffer);
                        if(integer == VERSION_NR)
                            state = CHECKSUM;
                        break;
            case CHECKSUM:
                        read_bytes(setup,_4_BYTES,currentBuffer,&socket);
                        integer = read_int(setup, currentBuffer);
                        state = TYPE;
                        break;
            case TYPE:
                        read_bytes(setup, _4_BYTES,currentBuffer,&socket);
                        integer = read_int(setup, currentBuffer);
                        type = integer;
                        state = LENGTH;
                        break;
            case LENGTH:
                        read_bytes(setup,_4_BYTES,currentBuffer,&socket);
                        integer = read_int(setup, currentBuffer);
                        length = integer;
                        switch(type){
                            case LOGIN: state = LOGIN;break;
                            case MESSAGE: state = MESSAGE; break;
                            case UPDATE : state = UPDATE; break;
                            case LOGOUT: state = LOGOUT; break;
                            default: state = VERSION; close(socket);
                        }
                        break;                  
            case LOGIN:
                        read_bytes(setup,length+1,currentBuffer,&socket);
                        integer =0;
                        for(i=0;i<BUFFLEN;i++){
                            if (strcmp(setup->buffer[currentBuffer].element, list[i].username)==0){
                                integer = USER_FOUND;
                                break;
                            }
                        }
                        if(integer!= USER_FOUND){
                            strcpy(list[next_name].username, setup->buffer[currentBuffer].element);
                            printf("logged in:%s\n", list[next_name].username);
                            fflush(stdout);
                            //state = UPDATE;
                            state= VERSION;
                        }else{
                            state= VERSION;
                        }
                          //  close(socket);
                        break;
            case MESSAGE:  
                        printf("entered message\n");
                        msg.ip=length;
                        read_bytes(setup,_4_BYTES,currentBuffer,&socket);
                        integer = read_int(setup, currentBuffer);
                        msg.port = integer;
                        printf("port:%d\n",integer);
                        read_bytes(setup,_4_BYTES,currentBuffer,&socket);
                        integer = read_int(setup, currentBuffer);
                        printf("count:%d\n",integer);
                        if(msg.cnt < integer){}
                            printf("printing\n");                            
                            read_bytes(setup,_4_BYTES,currentBuffer,&socket);
                            integer = read_int(setup, currentBuffer);
                            length = integer;
                            read_bytes(setup,length+1,currentBuffer,&socket);
                            printf("%s\n",setup->buffer[currentBuffer].element);
                            //state = UPDATE;
                            state = VERSION;
                        break;
            case UPDATE:  
                        broadcast_update(setup, &type, list, &next_name);
                        next_name = (next_name+1) % BUFFLEN;
                        state = VERSION;
                        break;
            case LOGOUT: 
                        break;
                                
        }

    }     
    pthread_exit(NULL);
 }

 void broadcast_update(config* setup, uint32_t* type, users* list, uint8_t* next){
       uint32_t integer;
       //sending version
       integer = VERSION_NR;
       setup->conn.error = write(setup->conn.sockfd,&integer,_4_BYTES);
       if (setup->conn.error < 0) sys_call_error("failed writing to the socket\n");
       //sending checksum
       integer = CHECKSUM;
       setup->conn.error = write(setup->conn.sockfd,&integer,_4_BYTES);
       if (setup->conn.error < 0) sys_call_error("failed writing to the socket\n");
       //sending type
       integer = UPDATE;
       setup->conn.error = write(setup->conn.sockfd,&integer,_4_BYTES);
       if (setup->conn.error < 0) sys_call_error("failed writing to the socket\n");
       //sending type of update
       integer = *(type);
       setup->conn.error = write(setup->conn.sockfd,&integer,_4_BYTES);
       if (setup->conn.error < 0) sys_call_error("failed writing to the socket\n");
       //sending IP origin of the update
       integer = setup->in_ip_addr;
       setup->conn.error = write(setup->conn.sockfd,&integer,_4_BYTES);
       if (setup->conn.error < 0) sys_call_error("failed writing to the socket\n");
       //sending portnumber of the update
       integer = setup->in_port;
       setup->conn.error = write(setup->conn.sockfd,&integer,_4_BYTES);
       if (setup->conn.error < 0) sys_call_error("failed writing to the socket\n");
       //sending length of the message
       integer = 20;
       setup->conn.error = write(setup->conn.sockfd,&integer,_4_BYTES);
       if (setup->conn.error < 0) sys_call_error("failed writing to the socket\n");
       //sending the actual freaking message itself..
       setup->conn.error = write(setup->conn.sockfd,list[*(next)].username,20);
       if (setup->conn.error < 0) sys_call_error("failed writing to the socket\n");
}
