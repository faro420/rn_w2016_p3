#@author: Mir Farshid Baha
#contact: mirfarshid.baha@haw-hamburg.de
#about: Makefile for the ultimate chat protocol
CC = /usr/bin/gcc
CFLAGS = -pthread -g -Wall -Wextra -Werror -I.

ucp:ucp.c
	$(CC) $(CFLAGS) -o ucp client.c server.c ucp.c utility_functions.c controller.c
clean:
	rm -f ucp

