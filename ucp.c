 /*
    @author: Mir Farshid Baha
    purpose: RNP-WS2016
    version: v0.9
*/

#include "ucp.h"

//declaration of synchronization elements
pthread_mutex_t mutex;
pthread_cond_t empty, full;
int active;
int err;

//declaration buffer related and variables
int put, take, fullEntries;


int main(int argc, char *argv[]){
    int i;
    pthread_t server_thread, client_thread;
    pthread_t controllers[BUFFLEN];
    config setup;
    //check if enough arguments were passed
    if (argc < 4) {
        fprintf(stderr,"usage %s host_ip in_port host_port \n", argv[0]);
        exit(0);
    }
    //initializing setup struct
    strcpy(setup.host_ip, argv[1]);
    setup.in_port = atoi(argv[2]);
    setup.host_port = atoi(argv[3]);
    //initializing global variables
    init(&setup);
    for(i=0; i<BUFFLEN; i++)
        bzero(setup.buffer[i].element,400);
    config* psetup =&setup;
    //creating server thread
    err=pthread_create(&server_thread,NULL,server,(void*)psetup);
    assert(!err);
    //creating client thread
    err=pthread_create(&client_thread,NULL,client,(void*)psetup);
    assert(!err);
    //creating controller threads
    for(i=0;i<BUFFLEN;i++){
    	err=pthread_create(&controllers[i],NULL,controller,(void*)psetup);
    	assert(!err);
    }
    
    //waiting for server and client threads to exit
    while(active ==1){}
    err=pthread_join(client_thread,NULL);
    assert(!err);
    err=pthread_join(server_thread,NULL);
    assert(!err);
    for(i=0;i<BUFFLEN;i++)
    	err=pthread_join(controllers[i],NULL);
    assert(!err);
    //destroying pthread elements
    cleanup();
    exit(EXIT_SUCCESS);
}
