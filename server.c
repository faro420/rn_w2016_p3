 /*
    @author: Mir Farshid Baha
    purpose: RNP-WS2016
    version: v0.9
*/
#include "ucp.h"


void* server(void*args){ 
    config * setup = ((config*)args);
    //declaration of required variables
     int sockfd, newsockfd, portno;
     socklen_t clilen;
     struct sockaddr_in serv_addr, cli_addr;
     //opening the socket
     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) 
       sys_call_error("failed to open the socket\n");
     bzero((char *) &serv_addr, sizeof(serv_addr));
     //setting port number
     portno = setup->in_port;
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(portno);
     setup->in_ip_addr = serv_addr.sin_addr.s_addr;
     //binding the socket
     if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) 
        sys_call_error("failed to bind the socket\n");
     //listening to socket
     listen(sockfd,5);
     clilen = sizeof(cli_addr);
     while(active==1){
        newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
        if (newsockfd < 0) sys_call_error("failed to accept\n");
        //receiving a message 
        pthread_mutex_lock(&mutex);   
        while (fullEntries == BUFFLEN) 
            pthread_cond_wait(&empty, &mutex);   
        setup->buffer[put].socket = newsockfd;
	setup->buffer[put].client_ip =cli_addr.sin_addr.s_addr;
	setup->buffer[put].client_port =cli_addr.sin_port;
        put = (put + 1) % BUFFLEN;   
        fullEntries++;             
        pthread_cond_broadcast(&full);             
        pthread_mutex_unlock(&mutex);
        //end of receiving message
    }
    close(newsockfd);
    close(sockfd);
    pthread_exit(NULL);
} 
