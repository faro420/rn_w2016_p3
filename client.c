 /*
    @author: Mir Farshid Baha
    purpose: RNP-WS2016
    version: v0.9
*/
#include "ucp.h"
#include<arpa/inet.h> //inet_addr

void* client(void*args){
    config * setup = ((config*)args);
    //declaration of required variables
    char buffer[400];
    //yay, now, let's write dem messages..
    fgets(buffer,399,stdin); 
    setup->conn.portno = setup->host_port;
    //opening the socket
    setup->conn.sockfd = socket(AF_INET, SOCK_STREAM, 0); if (setup->conn.sockfd < 0) sys_call_error("failed opening the socket\n");
    setup->conn.serv_addr.sin_addr.s_addr = inet_addr(setup->host_ip);
    setup->conn.serv_addr.sin_family = AF_INET;
    setup->conn.serv_addr.sin_port = htons(setup->conn.portno);
    if (connect(setup->conn.sockfd,(struct sockaddr *) &setup->conn.serv_addr,sizeof(setup->conn.serv_addr)) < 0) 
    sys_call_error("failed to connect\n");
    while(active==1){
        bzero(buffer,400);
        fgets(buffer,399,stdin);
        if(buffer[0]=='q' && buffer[1]=='\n'){
            active = 0;
            printf("In Hamburg sag man Tschuess!\n");
            fflush(stdout);
        }else if(buffer[0]=='l' && buffer[1]=='o' && buffer[2]=='g' && buffer[3]=='i' && buffer[4]=='n' && buffer[5]=='\n'){
            printf("loggin in:\n");
            fflush(stdout);                   
                    bzero(buffer,400);
                    fgets(buffer,399,stdin);
                    login(setup,buffer);
        }else if(buffer[0]=='l' && buffer[1]=='o' && buffer[2]=='g' && buffer[3]=='o' && buffer[4]=='u' && buffer[5]=='t' && buffer[6]=='\n'){
                   // logout(setup,buffer);
        }else{
            send_message(setup,buffer);
        }
        bzero(buffer,400);
    }
    close(setup->conn.sockfd);
    pthread_exit(NULL);
} 

void login(config* setup, char* buffer){
       uint32_t integer;
       //sending version
       integer = VERSION_NR;
       integer = htonl(integer);
       setup->conn.error = write(setup->conn.sockfd,&integer,_4_BYTES);
       if (setup->conn.error < 0) sys_call_error("failed writing to the socket\n");
       //sending checksum
       integer = CHECKSUM;
       integer = htonl(integer);
       setup->conn.error = write(setup->conn.sockfd,&integer,_4_BYTES);
       if (setup->conn.error < 0) sys_call_error("failed writing to the socket\n");
       //sending type
       integer = LOGIN;
       integer = htonl(integer);
       setup->conn.error = write(setup->conn.sockfd,&integer,_4_BYTES);
       if (setup->conn.error < 0) sys_call_error("failed writing to the socket\n");
       //sending length of the message
       integer = strlen(buffer)+1;
       integer = htonl(integer);
       setup->conn.error = write(setup->conn.sockfd,&integer,_4_BYTES);
       if (setup->conn.error < 0) sys_call_error("failed writing to the socket\n");
       //sending the actual freaking message itself..
       integer = strlen(buffer)+1;
       setup->conn.error = write(setup->conn.sockfd,buffer,integer);
       if (setup->conn.error < 0) sys_call_error("failed writing to the socket\n");
}

void send_message(config* setup, char* buffer){
      uint32_t integer;
       //sending version
       integer = VERSION_NR;
       integer = htonl(integer);
       setup->conn.error = write(setup->conn.sockfd,&integer,_4_BYTES);
       if (setup->conn.error < 0) sys_call_error("failed writing to the socket\n");
       //sending checksum
       integer = CHECKSUM;
       integer = htonl(integer);
       setup->conn.error = write(setup->conn.sockfd,&integer,_4_BYTES);
       if (setup->conn.error < 0) sys_call_error("failed writing to the socket\n");
       //sending type
       integer = MESSAGE;
       integer = htonl(integer);
       setup->conn.error = write(setup->conn.sockfd,&integer,_4_BYTES);
       if (setup->conn.error < 0) sys_call_error("failed writing to the socket\n");
       //sending the IP origin
       integer = setup->in_ip_addr;
       integer = htonl(integer);
       setup->conn.error = write(setup->conn.sockfd,&integer,_4_BYTES);
       if (setup->conn.error < 0) sys_call_error("failed writing to the socket\n");
       //sending the origin port
       integer = setup->in_port;
       integer = htonl(integer);
       setup->conn.error = write(setup->conn.sockfd,&integer,_4_BYTES);
       if (setup->conn.error < 0) sys_call_error("failed writing to the socket\n");
       //sending msg counter
       integer = 1;
       integer = htonl(integer);
       setup->conn.error = write(setup->conn.sockfd,&integer,_4_BYTES);
       if (setup->conn.error < 0) sys_call_error("failed writing to the socket\n");
       //sending message length
       integer = strlen(buffer)+1;
       integer = htonl(integer);
       setup->conn.error = write(setup->conn.sockfd,&integer,_4_BYTES);
       if (setup->conn.error < 0) sys_call_error("failed writing to the socket\n");
       //sending the actual freakin' message
       integer = strlen(buffer)+1;
       setup->conn.error = write(setup->conn.sockfd,buffer,integer);
       if (setup->conn.error < 0) sys_call_error("failed writing to the socket\n");   
}
